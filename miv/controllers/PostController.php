<?php

namespace app\controllers;

use Yii;
use app\models\post;
use app\models\postSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * PostrController implements the CRUD actions for post model.
 */
class PostController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [ // כאן מגדירים למי מותר להיכנס לאן
                'class' => AccessControl::className(),
                'only' => ['update','create', 'delete'], // חל רק על עדכון. ניתן להוסיף עוד בתוך הסוגריים עם הפרדת פסיקים
                'rules' => [
                    [
                        'allow' => true, //תאפשר
                        'actions' => ['update'], // לעשות עדכון
                        'roles' => ['updatePost'], // למי שיש את ההרשאה 
                        'roleParams' => function() {     //אם אין חוק, אין צורך בשתי השורות הבאות
                            return ['post' => Post::findOne(['id' => Yii::$app->request->get('id')])]; // רלוונטית רק להרשאה עם חוק
                                                                     // מעבירים פרמטר באיזה פוסט מדובר
                        },
                    ],
                     [
                        'allow' => true, //תאפשר
                        'actions' => ['create'],
                        'roles' => ['createPost'], 
                     ],
                     [
                        'allow' => true, //תאפשר
                        'actions' => ['delete'],
                        'roles' => ['deletePost'], 
                     ],
                ],
            ],
        ];            
    }

    /**
     * Lists all post models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new postSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single post model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    { // הוספת הרשאה שאוט'ר וסתם יוזר לא יוכלו לראות פוסטים שאינם מפורסמים משמע סטטוס 1
        $model = $this->findModel($id);
        if (!\Yii::$app->user->can('author') && $model->status ==1){
            throw new ForbiddenHttpException("Sorry you are not allowed to view unpublished posts");
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new post model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new post();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing post model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing post model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = post::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
